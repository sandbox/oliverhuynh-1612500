<?php

/**
 * Implements hook_rules_condition_info() on behalf of the user module.
 * TODO: Remove operation
 */
function apply_for_role_rules_rules_condition_info() {
  return array(
    'user_has_applied_role' => array(
      'label' => t('User has applied role(s)'),
      'parameter' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User')
        ),
        'roles' => array(
          'type' => 'list<integer>',
          'label' => t('Requested roles'),
          'options list' => 'rules_user_roles_options_list',
        ),
        'operation' => array(
          'type' => 'text',
          'label' => t('Match roles'),
          'options list' => 'rules_user_condition_operations',
          'restriction' => 'input',
          'optional' => TRUE,
          'default value' => 'AND',
          'description' => t('If matching against all selected roles, the user must have <em>all</em> the roles selected.'),
        ),
        'type' => array(
          'type' => 'list<text>',
          'label' => t('Approve status'),
          'options list' => 'apply_for_role_rules_approved_status_list',
          'restriction' => 'input',
        ),
      ),
      'group' => t('Apply For Roles'),
      'access callback' => 'rules_user_integration_access',
      'base' => 'apply_for_role_rules_user_has_applied_role',
    ),
  );
}

/**
 * Implements hook_rules_action_info() on behalf of the user module.
 */
function apply_for_role_rules_rules_action_info() {
  $defaults = array(
   'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'description' => t('The user whose roles should be changed.'),
        'save' => FALSE,
      ),
      'roles' => array(
        'type' => 'list<integer>',
        'label' => t('Requested roles'),
        'options list' => 'rules_user_roles_options_list',
      ),
    ),
    'group' => t('Apply For Roles'),
    'access callback' => 'rules_user_role_change_access',
  );
  $items['user_approve_role'] = $defaults + array(
    'label' => t('Approve user roles'),
    'base' => 'apply_for_role_rules_user_approve_role',
  );
  $items['user_apply_for_role'] = $defaults + array(
    'label' => t('Apply for a role for the user'),
    'base' => 'apply_for_role_rules_user_apply_for_role',
  );
  $items['user_apply_for_role']['parameter']['account']['description'] = t('The user who is applying for a role.');
  return $items;
}

/**
 * Condition user: condition to check whether user has applied particular roles
 * TODO: Remove operation
 */
function apply_for_role_rules_user_has_applied_role($account, $roles, $operation = 'AND', $type = "") {

  // Check if there is a none value
  foreach ($type as $t) {
    if (!is_numeric($t)) {
      $type = FALSE;
      break;
    }
  }

  $match_applying_roles = apply_for_role_rules_get_roles($account, is_array($type)  && count($type)? $type : FALSE);

  $ret = FALSE;
  switch ($operation) {
    case 'OR':
      $ret = FALSE;
      foreach ($roles as $rid) {
        if (isset($match_applying_roles[$rid])) {
          $ret = TRUE;
          break;
        }
      }
      break;

    case 'AND':
      $ret = TRUE;
      foreach ($roles as $rid) {
        if (!isset($match_applying_roles[$rid])) {
          $ret = FALSE;
          break;
        }
      }
      break;
  }

  return $ret;
}

/**
 * Get applied roles with specific conditions
 * $approved = FALSE: No filter
 *                = APPLY_FOR_ROLE_REMOVED/APPLY_FOR_ROLE_PENDING/APPLY_FOR_ROLE_APPROVED/APPLY_FOR_ROLE_DENIED
 */
function apply_for_role_rules_get_roles($user, $approved = FALSE) {
  $ret = array();

  $roles = user_roles(TRUE);

  if ($approved === FALSE || (count($approved) == 0)) {
    $result = db_query("SELECT rid FROM {users_roles_apply} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAll();
  }
  else {
    $result = db_query("SELECT rid FROM {users_roles_apply} WHERE uid = :uid AND approved IN (:approved)", array(':uid' => $user->uid, ':approved' => $approved))->fetchAll();
  }
  foreach ($result as $row) {
    if (isset($roles[$row->rid])) {
      $ret[$row->rid] = $roles[$row->rid];
    }
    else {
      continue;
    }
  }

  return $ret;
}

function apply_for_role_rules_approved_status_list() {
  return array(
    '' => 'None',
    APPLY_FOR_ROLE_REMOVED => 'APPLY_FOR_ROLE_REMOVED',
    APPLY_FOR_ROLE_PENDING => 'APPLY_FOR_ROLE_PENDING',
    APPLY_FOR_ROLE_APPROVED => 'APPLY_FOR_ROLE_APPROVED',
    APPLY_FOR_ROLE_DENIED => 'APPLY_FOR_ROLE_DENIED',
  );
}

/**
 * Action: Approve roles to a particular user.
 */
function apply_for_role_rules_user_approve_role($account, $roles) {
  if ($account->uid) {
    $roles = array_unique($roles);

    foreach ($roles as $rid) {
      apply_for_role_approve_apply($account, $rid);
    }
  }
  watchdog('apply_for_role_rules', 'Approve <pre>'. var_export(array($account->uid, $account->name, $roles), TRUE) .'</pre>');
}

/**
 * Action: Apply for a role for particular user.
 */
function apply_for_role_rules_user_apply_for_role($account, $roles) {
  if ($account->uid) {
    $roles = array_unique($roles);

    foreach ($roles as $rid) {
      apply_for_role_add_apply($account, $rid);
    }
  }
  watchdog('apply_for_role_rules', 'Apply <pre>'. var_export(array($account->uid, $account->name, $roles), TRUE) .'</pre>');
}

/**
 * Implements hook_rules_event_info().
 *
 * Provide Rules events for Apply for role actions.
 */
function apply_for_role_rules_rules_event_info() {
  $apply_for_role_events = array('apply', 'approve', 'deny', 'remove');
  $rules_events = array();
  foreach ($apply_for_role_events as $event) {
    $rules_events['apply_for_role_rules_event_' . $event] = array(
      'label' => t("Apply for role (" . $event . ")"),
      'group' => t('Apply for role'),
      'variables' => array(
        'account' => array('type' => 'user', 'label' => t('User that the action was performed to')),
        'action' => array('type' => 'text', 'label' => t('Action performed')),
        'apply' => array('type' => 'object', 'label' => t('Apply for role objet')),
      ),
    );
  }
  return $rules_events;
}
